
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

   var fsconnect;

  @override
  void initState() {
    try {
      fsconnect = FirebaseFirestore.instance;
    print(fsconnect);
    } catch(E){
      print(E);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new CustomScrollView(
        slivers: [
          new SliverAppBar()
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            label: "Home"
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.outbond),
            label: "Test",
          )
        ],
      ),
    );
  }
}
