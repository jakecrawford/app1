import 'package:app1/views/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

FirebaseApp app;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final String name = "app1";
  final FirebaseOptions firebaseOptions = const FirebaseOptions(
    appId: '1:570276789324:ios:5ff1e6a8701ba1db3648e9',
    apiKey: 'AIzaSyAJx4DRiMVwgBEiDjukwANfl365qUzSRio',
    projectId: 'app1-9ae7e',
    messagingSenderId: '570276789324',
  );
  app = await Firebase.initializeApp(name: name, options: firebaseOptions);
  //await FirebaseAuth.instance.signInAnonymously();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Health App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}
